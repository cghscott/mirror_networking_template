using Mirror;
using NetworkRules;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ServerManager : NetworkManager
{

    //public event Action<ServerClient> addPlayerTrigger;

    public override void OnStartServer()
    {
        base.OnStartServer();

        //addPlayerTrigger += AddClient;
    }

    public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    {
        base.OnServerAddPlayer(conn);

        //GameObject player;

        ////From base.OnServerAddPlayer - to grab the game object:
        //{
        //    Transform startPos = GetStartPosition();
        //    player = startPos != null
        //        ? Instantiate(playerPrefab, startPos.position, startPos.rotation)
        //        : Instantiate(playerPrefab);
        //    player.name = $"{playerPrefab.name} [connId={conn.connectionId}]";
        //    NetworkServer.AddPlayerForConnection(conn, player);
        //}

        //ServerClient client = player.GetComponent<ServerClient>();
        //client.CreateConnection(conn);

        ////addPlayerTrigger.Invoke(client);
        //AddClient(client);
    }

    //private void AddClient(ServerClient client)
    //{
    //    clients.Add(client);
    //}
}
