using Mirror;
using NetworkRules;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.MemoryProfiler;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class ServerClient : NetworkBehaviour
{
    [SerializeField] private Player player;
    private NetworkConnectionToClient connection = null;

    public override void OnStartClient()
    {
        base.OnStartClient();
        player = GetComponent<Player>();
        CmdToServer();
    }

    public void CreateConnection(NetworkConnectionToClient newConnection)
    {
        if (connection != null)
        {
            Debug.LogError("Error! " + this.name + " already has a connection");
            return;
        }

        connection = newConnection;
        // CmdJustYou(); Needs two players to implement
    }

    [Command]
    public void CmdToServer()
    {
        RpcAllOfUs();
        TargetJustMe();
    }

    [Command]
    public void CmdJustYou(GameObject target)
    {
        NetworkIdentity targetID = target.GetComponent<NetworkIdentity>();
        TargetJustYou(targetID.connectionToClient);
    }

    [ClientRpc]
    public void RpcAllOfUs()
    {
        player.AllOfUs();
    }

    [TargetRpc]
    public void TargetJustMe()
    {
        player.JustMe();
    }

    [TargetRpc]
    public void TargetJustYou(NetworkConnectionToClient target)
    {
        player.JustMe();
    }
}
