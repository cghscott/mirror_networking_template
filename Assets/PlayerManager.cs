using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerManager : NetworkBehaviour
{
    [SerializeField] private GameObject redLight;
    [SerializeField] private GameObject greenLight;
    private GameObject _light;
    public List<GameObject> _lights;
    private Transform _lightEmitter;


    public override void OnStartClient()
    {
        base.OnStartClient();

    }



    [Command]
    public void SetLightEmitter(Transform lightEmitter)
    {
        _lightEmitter = lightEmitter;
    }

    [Command]
    public void CmdChangeColour(Transform lightEmitter)
    {
        if (isOwned)
        {
            _light = Instantiate(greenLight);
            NetworkServer.Spawn(_light, connectionToClient);
        }
        else
        {
            _light = Instantiate(redLight);
            NetworkServer.Spawn(_light, connectionToClient);
        }

        _lights.Add(_light);
        RpcShowLight(_light, lightEmitter);
        RpcClearOtherColours();
    }

    [Command]
    public void CmdColourOff()
    {
        RpcDestroyLights();
    }

    [Command]
    public void CmdShowLight(Transform lightEmitter)
    {
        if (_light != null)
        {
            RpcShowLight(_light, lightEmitter);
        }
    }

    [ClientRpc]
    void RpcDestroyLights()
    {
        if (_lights.Count > 0)
        {
            foreach (GameObject light in _lights)
            {
                NetworkServer.Destroy(light);
            }
            _lights = new List<GameObject>();
        }
    }

    [ClientRpc]
    void RpcClearOtherColours()
    {
        if (_lights.Count > 1)
        {
            for (int i = 0; i < _lights.Count - 1; i++)
            {
                NetworkServer.Destroy(_lights[i]);
            }
        }
    }

    [ClientRpc]
    void RpcShowLight(GameObject light, Transform lightEmitter)
    {
        light.transform.SetParent(lightEmitter, false);
    }
}
