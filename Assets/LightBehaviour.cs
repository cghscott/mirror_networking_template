using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class LightBehaviour : NetworkBehaviour
{
    public override void OnStartClient()
    {
        base.OnStartClient();

        //NetworkIdentity networkIdentity = NetworkClient.connection.identity;
        //PlayerManager playerManager = networkIdentity.GetComponent<PlayerManager>();
        //playerManager.CmdShowLight(gameObject);
    }

    [Command]
    public void CmdSetParent()
    {
        NetworkIdentity networkIdentity = NetworkClient.connection.identity;
        GameObject emitter = networkIdentity.GetComponent<ChangeColour>().gameObject;
        transform.SetParent(emitter.transform, false);
        //Call Rpc
        RpcSetParent(networkIdentity);
    }

    [ClientRpc]
    public void RpcSetParent(NetworkIdentity networkIdentity)
    {
        GameObject emitter = networkIdentity.GetComponent<ChangeColour>().gameObject;
        transform.SetParent(emitter.transform, false);
    }
}
