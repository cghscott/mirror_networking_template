using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;

public class NetworkManagerCardGame : NetworkManager
{
    [SerializeField] ChangeColour changeColour;

    public event Action addPlayerTrigger;

    public override void OnStartServer()
    {
        base.OnStartServer();

        addPlayerTrigger += () => changeColour.ShowColour();
    }

    public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    {
        base.OnServerAddPlayer(conn);

        addPlayerTrigger.Invoke();
    }
}
