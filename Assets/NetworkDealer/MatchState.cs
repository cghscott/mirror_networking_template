using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RulesFramework
{
    // Perhaps should be static class and not inherit
    public class MatchState : MonoBehaviour
    {
        private List<Player> players = new List<Player>();

        // Probably should be in inheriting implementation
        private List<Player> activePlayers = new List<Player>();
        private List<Player> inactivePlayers = new List<Player>();

        public void SetActivePlayers(List<int> playerIndices)
        {
            activePlayers.Clear();
            inactivePlayers.Clear();

            for (int i = 0; i < players.Count; i++)
            {
                if (playerIndices.Contains(i))
                {
                    activePlayers.Add(players[i]);
                }
                else
                {
                    inactivePlayers.Add(players[i]);
                }
            }
        }
    }
}