using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RulesFramework
{
    public class CardEffect
    {
        public void Apply()
        {

        }
    }

    public class CardCommand
    {
        private List<CardEffect> effects;
        
        public void AddEffect(CardEffect effect)
        {
            effects.Add(effect);
        }

        public void Execute()
        {
            foreach (var effect in effects)
            {
                effect.Apply();
            }
        }
    }

    [CreateAssetMenu(fileName = "New Card", menuName = "Card")]
    public class Card : ScriptableObject
    {
        [SerializeField] public string cardName;
        [SerializeField] public string description;

        private List<CardCommand> useCommands;

        public void Initialise()
        {

        }

        public void UseCard()
        {
            foreach(var command in useCommands)
            {
                command.Execute();
            }
        }


    }
}
