using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

namespace NetworkRules
{
    public class StepManagerNetwork : NetworkBehaviour
    {
        [SerializeField] private RulesFramework.StepManager stepManager;
        [SerializeField] private List<ClientMessenger> clients;

        [SerializeField] private List<ClientMessenger> unreadyClients;

        public void AddPlayer(ClientMessenger client)
        {
            clients.Add(client);
        }

        [Command]
        public void CmdAddPlayerStepData(ClientMessenger client)
        {
            unreadyClients.Remove(client);

            if(unreadyClients.Count == 0)
            {
                CmdAdvanceStep();
            }
        }
        
        [Command]
        void CmdAdvanceStep()
        {
            stepManager.AdvanceStep();
        }

        [Command]
        void CmdUpdateMatchState()
        {

        }
    }
}