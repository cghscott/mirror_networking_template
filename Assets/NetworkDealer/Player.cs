using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RulesFramework
{
    public class Player : MonoBehaviour
    {
        [SerializeField] private DeckConstruction fullDeck;

        [SerializeField] public DeckBattle deckBattle;

        private void Awake()
        {
            fullDeck.Initialise();
            deckBattle.InitialiseDrawPile(fullDeck.cards);
        }
    }
}
