using RulesFramework;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RulesFramework
{
    public class CardDisplay : MonoBehaviour
    {
        [SerializeField] private Card card;

        [SerializeField] private TextMeshPro nameText;
        [SerializeField] private TextMeshPro descriptionText;

        public void Initialise(Card card)
        {
            this.card = card;
            nameText.text = card.cardName;
            descriptionText.text = card.description;
            this.card.Initialise();
        }
    }
}
