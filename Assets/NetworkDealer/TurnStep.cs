using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RulesFramework
{
    public abstract class TurnStep : MonoBehaviour
    {
        public event Action beginStepTrigger;
        public event Action endStepTrigger;

        internal void BeginStep()
        {
            Initialise();
            beginStepTrigger?.Invoke();
        }

        protected void EndStep()
        {
            endStepTrigger?.Invoke();
        }

        protected abstract void Initialise();
    }
}