using RulesFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RulesFramework
{
    public class StepSample : TurnStep
    {
        [SerializeField] private GameObject drawButton; 

        protected override void Initialise()
        {
            drawButton.SetActive(true);
        }
    }
}
