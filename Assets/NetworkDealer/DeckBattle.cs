using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RulesFramework
{
    public class DeckBattle : MonoBehaviour
    {
        private List<Card> drawPile = new List<Card>();

        [SerializeField] private CardDisplay cardPrefab;

        private List<Card> hand = new List<Card>();

        private List<Card> discardPile = new List<Card>();

        private List<Card> exhaustPile = new List<Card>();

        public void InitialiseDrawPile(List<Card> cards)
        {
            drawPile = cards;
            ShuffleDrawPile();
        }

        public void ShuffleDrawPile()
        {
            System.Random random = new System.Random();
            drawPile = drawPile.OrderBy(i => random.Next(0, drawPile.Count)).ToList();
        }

        private void SetupEffects()
        {

        }

        public void DrawCards(int count)
        {
            for (int i = 0; i < count; i++)
            {
                cardPrefab.Initialise(drawPile[i]);
            }
        }

        private void DiscardCards(int count)
        {

        }

        private void ExhaustCards(int count)
        {

        }
    }
}
