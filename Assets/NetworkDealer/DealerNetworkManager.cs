using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;

namespace NetworkRules
{
    public class DealerNetworkManager : Mirror.NetworkManager
    {
        [SerializeField]
        private StepManagerNetwork stepManagerNetwork;

        public event Action<ClientMessenger> addPlayerTrigger;
        public override void OnStartServer()
        {
            base.OnStartServer();
            addPlayerTrigger += AddToClientList;
        }

        public override void OnServerAddPlayer(NetworkConnectionToClient conn)
        {
            GameObject player;

            //From base.OnServerAddPlayer - to grab the game object:
            {
                Transform startPos = GetStartPosition();
                player = startPos != null
                    ? Instantiate(playerPrefab, startPos.position, startPos.rotation)
                    : Instantiate(playerPrefab);
                player.name = $"{playerPrefab.name} [connId={conn.connectionId}]";
                NetworkServer.AddPlayerForConnection(conn, player);
            }

            ClientMessenger client = player.GetComponent<ClientMessenger>();
            client.CreateConnection(stepManagerNetwork, conn);
            addPlayerTrigger.Invoke(client);            
        }

        private void AddToClientList(ClientMessenger client)
        {
            stepManagerNetwork.AddPlayer(client);
        }
    }
}