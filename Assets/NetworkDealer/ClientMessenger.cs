using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

namespace NetworkRules
{
    public class ClientMessenger : Mirror.NetworkBehaviour
    {
        private NetworkConnectionToClient connection = null;
        StepManagerNetwork stepManagerNetwork;
        PlayerTurnData turnData;

        public void CreateConnection(StepManagerNetwork stepManager, NetworkConnectionToClient newConnection) 
        {
            if (connection != null)
            {
                Debug.LogError("Error! " + this.name + " already has a connection");
                return;
            }

            stepManagerNetwork = stepManager;
            connection = newConnection;
        }

        public void AddPlayerStepData(PlayerTurnData data)
        {
            turnData = data;
            stepManagerNetwork.CmdAddPlayerStepData(this);
        }
    }
}