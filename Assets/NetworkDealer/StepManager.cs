using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RulesFramework
{
    public class StepManager : MonoBehaviour
    {
        private MatchState matchState;

        [SerializeField] private List<TurnStep> stepSequence = new List<TurnStep>();
        private int currentStepIndex = 0;

        private void Start()
        {
            matchState = new MatchState();
            stepSequence[currentStepIndex].BeginStep();
        }

        public void AdvanceStep()
        {
            IncrementStepIndex();
            stepSequence[currentStepIndex].BeginStep();
        }

        private void IncrementStepIndex()
        {
            currentStepIndex++;

            if (currentStepIndex >= stepSequence.Count)
            {
                currentStepIndex = 0;
            }
        }

        private void JumpToStep(int step)
        {
            currentStepIndex = step;
            stepSequence[currentStepIndex].BeginStep();
        }
    }
}