using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RulesFramework
{
    public class DeckConstruction : MonoBehaviour
    {
        public List<Card> cards { get; private set; }

        [SerializeField] private Card exampleCard;

        public void Initialise() 
        {
            cards = new List<Card>();
            cards.Add(exampleCard);
        }
    }
}
