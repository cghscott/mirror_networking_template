using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class ChangeColour : NetworkBehaviour
{
    public PlayerManager playerManager;
    public NetworkManager networkManager;

    public override void OnStartClient()
    {
        base.OnStartClient();
    }

    public void ShowColour()
    {
        NetworkIdentity networkIdentity = NetworkClient.connection.identity;
        playerManager = networkIdentity.GetComponent<PlayerManager>();
        playerManager.CmdShowLight(transform);
    }

    public void ColourTrigger()
    {
        NetworkIdentity networkIdentity = NetworkClient.connection.identity;
        playerManager = networkIdentity.GetComponent<PlayerManager>();
        playerManager.CmdChangeColour(transform);
    }

    public void OffTrigger()
    {
        NetworkIdentity networkIdentity = NetworkClient.connection.identity;
        playerManager = networkIdentity.GetComponent<PlayerManager>();
        playerManager.CmdColourOff();
    }
}
